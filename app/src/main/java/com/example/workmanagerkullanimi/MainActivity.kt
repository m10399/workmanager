package com.example.workmanagerkullanimi

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.workmanagerkullanimi.databinding.ActivityMainBinding
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.buttonYap.setOnClickListener {
            val calismaKosulu = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            val istek = OneTimeWorkRequestBuilder<MyWorker>().setConstraints(calismaKosulu).build()
            WorkManager.getInstance(this).enqueue(istek)
            WorkManager.getInstance(this).getWorkInfoByIdLiveData(istek.id).observe(this){
                val durum = it.state.name
                Log.e("Arkaplan işlem durumu",durum)
            }
        }
    }
}